import boto3

# TODO: Need to make this instance specific, currently pulling all volumes.
# instanceId = input("What is the instance ID?") 

ec2 = boto3.resource('ec2')
volumes = ec2.volumes.all() # List out all volumes

for volume in volumes:
    volume.create_snapshot(
    Description='My snapshot',
    TagSpecifications=[],
    DryRun=False)
